import React, { Component } from 'react';
import './App.css';
import Items from "./Items/Items";
import Form from "./Form/Form";

class App extends Component {
  state = {
    items:[],
    inputName: '',
    inputCost: '',
    totalSpent: 0
  };

    changeName = (event) => {
      this.setState({inputName:event.target.value})
    };

    changeCost = (event) => {
      this.setState({inputCost:event.target.value})
    };
    add = () => {
      const items = [...this.state.items];
      const newItem = {name: this.state.inputName, cost:this.state.inputCost};
      items.push(newItem);
      this.setState({items});
      const oldSpent = this.state.totalSpent;
      const newSpent = oldSpent + parseInt(this.state.inputCost);
      this.setState({totalSpent: newSpent});

    };

    remove = (index) => {
      const items = [...this.state.items];
      items.splice(index, 1);
      this.setState({items});
      const oldSpent = this.state.totalSpent;
      const newSpent = oldSpent - this.state.inputCost;
      this.setState({totalSpent: newSpent});
    };


  render() {
    return (
      <div className="App">
        <Form
          changeName={this.changeName}
          changeCost={this.changeCost}
          add={this.add}
        />
        <Items
        items={this.state.items}
        remove={this.remove}
        />
        <p >Total Spent: {this.state.totalSpent} </p>


      </div>
    );
  }
}

export default App;
