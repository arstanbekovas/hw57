import React, {Fragment} from 'react';
import './Form.css'

const Form = props => {
    return (
        <Fragment>
            <input className="Name" type="text" value={props.inputName} placeholder="Item name" onChange={props.changeName}/>
            <input className="Cost" type="text" value={props.inputCost} placeholder="Cost" onChange={props.changeCost}/><span>KGS</span>
            <button className="Add" onClick={props.add}>Add</button>
        </Fragment>
    )
};

export default Form;