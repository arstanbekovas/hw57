import React from 'react';
import './Item.css';

const Item = props => {
    return (
        <div className="Item">
            <span className="Name">{props.name}</span>
            <span className='Cost'>{props.cost} KGS</span>
            <button className="Remove" onClick={props.remove}>X</button>

        </div>
    );
};

export default Item;