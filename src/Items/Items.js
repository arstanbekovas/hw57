import React from 'react';
import Item from "./Item/Item";
import './Items.css'

const Items = props => {
    return (
        <div className="Items">
            {props.items.map((item, index) => {
               return <Item
                   key={index}
                   name={item.name}
                   cost={item.cost}
                   remove={() => props.remove(index) }
               />

            })}
        </div>

    )

};

export default Items;